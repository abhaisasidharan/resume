from django.contrib import admin

from .models import CvSection, CvItem, Starwars, StarwarsQuote

admin.site.register(CvSection)
admin.site.register(CvItem)
admin.site.register(Starwars)
admin.site.register(StarwarsQuote)
