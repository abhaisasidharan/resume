from django.db import models
from django.utils import timezone
import datetime

class CvSection(models.Model):
    cv_heading = models.CharField(max_length=200)

    def __str__(self):
        return self.cv_heading


class CvItem(models.Model):
    cv_section = models.ForeignKey(CvSection)
    cv_item_left = models.CharField(max_length=200)
    cv_item_right = models.TextField()

    def __str__(self):
        return self.cv_item_left

    def get_heading(cv_section_id):
        cv_section = CvSection.objects.filter(id=cv_section_id)[:1].get()
        return cv_section.cv_heading

    def get_cv_item(cvs_id):
        cv_item = {}
        cvi_object = []
        cv_items = CvItem.objects.filter(cv_section_id=cvs_id)
        for cvi in cv_items:
            cv_item = {cvi.cv_item_left: cvi.cv_item_right}
            cvi_object.append(cv_item)
        return cvi_object

class StarwarsQuote(models.Model):
    quote = models.TextField()

    def __str__(self):
        return self.quote

class Starwars(models.Model):
    starwars_quote = models.ForeignKey(StarwarsQuote)
    quote_date = models.DateTimeField()

    def __str__(self):
        return self.starwars_quote.quote

    def was_recently_updated(self):
        return self.quote_date >= timezone.now() - datetime.timedelta(days=7)
        #return (timezone.now() <= (self.quote_date() + datetime.datetime.timedelta(days=7)))
