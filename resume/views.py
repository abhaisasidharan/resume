from django.shortcuts import render
from django.http import HttpResponse
from django.template import loader
from django.utils import timezone
from lxml import html
import requests
from .models import CvSection, CvItem, StarwarsQuote, Starwars

def resume(request):
    cvi_objects = {}
    cv = []
    for i in range(1,10):
        #cv_sections.append(CvItem.get_heading(i))
        #cvi_objects.append(CvItem.get_cv_item(i))
        cvi_object = {CvItem.get_heading(i): CvItem.get_cv_item(i)}
        cv.append(cvi_object)
    template = loader.get_template('resume/resume.html')
    #return render(request, 'resume.html', {'cv_sections': cv_sections})
    return HttpResponse(template.render({'cv': cv}, request))

def home(request):
    template = loader.get_template('resume/home.html')
    quotes = getstarwarsquote()

    return HttpResponse(template.render({'quotes': quotes}, request))

def getstarwarsquote():
    sw = Starwars.objects.all()[:1]
    if not sw:
        quotes = crawl_and_scrap()
        quotes = list(Starwars.objects.all())
    else:
        sw = Starwars.objects.all()[:1].get()
        if sw.was_recently_updated() :
            quotes = list(Starwars.objects.all())
        else:
            Starwars.objects.all().delete()
            quotes = crawl_and_scrap()
    return quotes

def crawl_and_scrap():
    page = requests.get('http://www.starwars.com/news/15-star-wars-quotes-to-use-in-everyday-life')
    tree = html.fromstring(page.content)
    crawled_quotes = tree.xpath('//section[@class="entry-content"]/p/strong/text()')
    quotes = []
    for quote in crawled_quotes:
        if (not quote) or (quote.isspace()):
            continue
        else:
            swq = StarwarsQuote(quote=quote)
            swq.save()
            sw = Starwars(starwars_quote=swq, quote_date=timezone.now())
            sw.save()
            quotes.append(quote)
    return quotes
