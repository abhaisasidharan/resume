from django.db import models
from django.contrib.auth.models import User

# Create your models here.
class Chat(models.Model):
    user = models.ForeignKey(User)
    chat_text = models.TextField()
    timestamp = models.DateTimeField()
