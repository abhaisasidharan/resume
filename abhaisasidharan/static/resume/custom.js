$(document).ready(function(){

  var saberup = document.getElementById('saberup');
  var saberdown = document.getElementById('saberdown');

  $("a#cv-title, input.force").click(function(){
    var unhide = $(this).parents().get(2);
    $(unhide).find(".force").trigger("click");
    if ($(unhide).find(".force").is(":checked")) {
      $(unhide).find("div.cv-items").slideDown();
      saberup.play();
    }
    else {
      $(unhide).find("div.cv-items").slideUp();
      saberdown.play();
    }
  });
});
